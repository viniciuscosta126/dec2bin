import React,{useState} from "react";
import "./App.css";
const App = () => {
  const [input,setInput] = useState(0)
  const [resultado,setResultado] = useState(0)

  const handleInput = (event) =>{
    setInput(event.target.value)
    convertToBinary1(input)
  }

  function convertToBinary1 (number) {
    let num = number;
    let binary = (num % 2).toString();
    for (; num > 1; ) {
        num = parseInt(num / 2);
        binary =  (num % 2) + (binary);
    }
    setResultado(binary)
}
  return (
    <div className="container">
      <div>
        <label className="label">Digite o numero Decimal</label>
        <input onChange={handleInput} value={input} type="number" />
      </div>
      <h2>Resultado:{resultado}</h2>
    </div>
  );
};

export default App;
